package;


import cpp.Lib;


class Test {
	
	
	
	public static function printf (message:String):Void {
		
		test_call_printf (message);
		
	}
	
	
	public static function twoPlusTwo ():Int {
		
		return test_two_plus_two ();
		
	}
	
	
	private static var test_call_printf = Lib.load ("test", "test_call_printf", 1);
	private static var test_two_plus_two = Lib.load ("test", "test_two_plus_two", 0);
	
	
}