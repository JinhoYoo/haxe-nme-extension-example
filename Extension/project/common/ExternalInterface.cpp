#ifndef IPHONE
#define IMPLEMENT_API
#endif

#if defined(HX_WINDOWS) || defined(HX_MACOS) || defined(HX_LINUX)
#define NEKO_COMPATIBLE
#endif


#include <hx/CFFI.h>
#include "Utils.h"
#include <stdio.h>

using namespace test;



static void test_call_printf (value message) {
	
	printf (val_string (message));
	
}
DEFINE_PRIM (test_call_printf, 1);


static value test_two_plus_two () {
	
	return alloc_int (TwoPlusTwo ());
	
}
DEFINE_PRIM (test_two_plus_two, 0);



extern "C" void test_main () {
	
	// Here you could do some initialization, if needed
	
}
DEFINE_ENTRY_POINT (test_main);

extern "C" int test_register_prims () { return 0; }