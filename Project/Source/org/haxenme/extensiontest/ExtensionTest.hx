package org.haxenme.extensiontest;


import nme.display.Sprite;
import nme.display.StageAlign;
import nme.display.StageScaleMode;
import nme.Lib;
import nme.text.TextField;
import nme.text.TextFormat;
import Test;

/**
 * @author Joshua Granick
 */
class ExtensionTest extends Sprite {
	
	
	private var Label:TextField;
	
	
	public function new () {
		
		super ();
		
		initialize ();
		construct ();
		
	}
	
	
	private function construct ():Void {
		
		Label.defaultTextFormat = new TextFormat ("_sans", 24, 0x222222);
		Label.width = 400;
		Label.x = 10;
		Label.y = 20;
		Label.selectable = false;
		Label.text = "Two plus two equals " + Test.twoPlusTwo ();
		addChild (Label);
		
	}
	
	
	private function initialize ():Void {
		
		Lib.current.stage.align = StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = StageScaleMode.NO_SCALE;
		
		Label = new TextField ();
		
	}
	
	
	
	
	// Entry point
	
	
	
	
	public static function main () {
		
		Lib.current.addChild (new ExtensionTest ());
		
	}
	
	
}